$(document).ready(function () {
  console.log("ready");
  // красивый select
  if ($(".js-select").length) {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
      // closeOnSelect: false,
    });
  }
  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  $("input").on("change", function () {
    console.log($(this).val());
    if ($(this).val().length) {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
  // /маска для инпупов
  $(window).resize(function () {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
    });
  });
  // /красивый select
  // меню
  $(".js-toggle-service").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
    $(".js-service-menu").fadeToggle();
    if ($(this).hasClass("active")) {
      $("body").addClass("fixed");
    } else {
      $("body").removeClass("fixed");
    }
  });
  // меню каталога
  $(".js-catalog-opener").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
    if ($(window).width() > 1199){
      $(".js-catalog-content").fadeToggle();
    } else {
      $(".js-adaptive-menu-content").fadeToggle();
    }
    if ($(this).hasClass("active")) {
      $("body").addClass("fixed");
    } else {
      $("body").removeClass("fixed");
    }
  });
  //
  $(".js-adaptive-menu-opener").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
    $(".js-service-menu").fadeToggle();
    if ($(this).hasClass("active")) {
      $("body").addClass("fixed");
    } else {
      $("body").removeClass("fixed");
    }
  });
  //
  $(".js-popup-close").click(function () {
    parent.jQuery.fancybox.close();
  });

  if ($(".js-main-slider").length) {
    var swiper = new Swiper(".js-main-slider", {
      slidesPerView: 1,
      pagination: {
        el: ".js-main-slider-pagination",
        type: "fraction",
      },
      navigation: {
        nextEl: ".js-main-slider-next",
        prevEl: ".js-main-slider-prev",
      },
    });
  }
  // Отзывы
  if ($(".js-reviews-slider").length) {
    var swiper = new Swiper(".js-reviews-slider", {
      slidesPerView: 2,
      spaceBetween: 20,
      pagination: {
        el: ".js-reviews-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".js-reviews-next",
        prevEl: ".js-reviews-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        600: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        767: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      },
    });
  }

  $(".js-geography-name").on("click", function () {
    if (!$(this).parents(".js-geography-country").hasClass("active")) {
      $(".js-geography-country.active").find(".js-geography-ul").toggle();
      $(".js-geography-country.active").toggleClass("active");
    }
    $(this).parents(".js-geography-country").toggleClass("active");
    $(this).parents(".js-geography-country").find(".js-geography-ul").toggle();
  });

  $(".js-faq-item").on("click", function () {
    $(this).toggleClass("open");
    $(this).find(".js-faq-answer").slideToggle();
  });

  // карта
  ymaps.ready(function () {
    let width = 115;
    let height = 133;
    if ($(window).width() < 1200) {
      width = 60;
      height = 70;
    }
    var myMap = new ymaps.Map(
        "map",
        {
          center: [54.775947, 32.102533],
          zoom: 16,
          controls: [],
        },
        {}
      ),
      myPlacemark = new ymaps.Placemark(
        myMap.getCenter(),
        {},
        {
          iconLayout: "default#image",
          iconImageHref: "D:/фронтенд/жд/img/icons/map-icon.svg",
          iconImageSize: [width, height],
          iconImageOffset: [-width / 2, -height],
        }
      );
    myMap.geoObjects.add(myPlacemark);


    $('.js-contact-show').on('click', function(e){
      e.preventDefault();
      const coords = this.dataset.coords.split(', ');
      if(coords){
        showOnMap(coords);
      }
    })

    function showOnMap(coords) {
      myMap.geoObjects.removeAll();
      const myPlacemark = new ymaps.Placemark(coords, {}, {
          iconLayout: "default#image",
          iconImageHref: "D:/фронтенд/жд/img/icons/map-icon.svg",
          iconImageSize: [width, height],
          iconImageOffset: [-width / 2, -height],
        });
      myMap.geoObjects.add(myPlacemark);
      myMap.setCenter(coords);
    }

  });
  $(".js-scroll-top").on("click", function () {
    $("body, html").animate({ scrollTop: 0 }, 500);
  });

  $(window).scroll(function () {
    if ($(window).scrollTop() > 0) {
      $(".header").addClass("fix-header");
    } else {
      $(".header").removeClass("fix-header");
    }

    if ($(window).scrollTop() > 200) {
      $(".js-scroll-top").addClass("is-show");
    } else {
      $(".js-scroll-top").removeClass("is-show");
    }
  });
  $(".js-gondolas-more").on("click", function () {
    $(this)
      .parents(".js-gondolas-txt")
      .find(".js-gondolas__item-text")
      .addClass("active");
    $(this).parents(".js-gondolas-txt").find(".js-gondolas-hide").show();
    $(this).hide();
  });
  $(".js-gondolas-hide").on("click", function () {
    $(this)
      .parents(".js-gondolas-txt")
      .find(".js-gondolas__item-text")
      .removeClass("active");
    $(this).parents(".js-gondolas-txt").find(".js-gondolas-more").show();
    $(this).hide();
  });
  if ($(".js-gondolas-slider").length) {
    var swiper = new Swiper(".js-gondolas-slider", {
      pagination: {
        el: ".js-gondolas-pagination",
      },
      navigation: {
        nextEl: ".js-gondolas-next",
        prevEl: ".js-gondolas-prev",
      },
    });
  }

  $(".js-employee-contacts-show").on("click", function () {
    $(".employee-contacts__item").slideDown();
    $(this).hide();
  });

  // Благодарности
  if ($(".js-gratitude").length) {
    var swiper = new Swiper(".js-gratitude", {
      slidesPerView: 3,
      spaceBetween: 20,
      pagination: {
        el: ".js-gratitude-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".js-gratitude-next",
        prevEl: ".js-gratitude-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        766: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        600: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        980: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1190: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
        1200: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
      },
    });
  }

  // История компании
  $(".js-histoty-tab").on("click", function (e) {
    e.preventDefault();
    $(".js-histoty-tab.active").removeClass("active");
    $(this).addClass("active");
    let href = $(this).attr("href");
    $(".histoty__content.active").fadeOut().removeClass("active");
    $(href).fadeIn().addClass("active");
  });

  // Наша команда
  if ($(".js-team").length) {
    var swiper = new Swiper(".js-team", {
      slidesPerView: 4,
      spaceBetween: 20,
      pagination: {
        el: ".js-team-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".js-team-next",
        prevEl: ".js-team-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        766: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        980: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
        1200: {
          slidesPerView: 4,
          spaceBetween: 20,
        },
      },
    });
  }


  if ($(".js-photos").length) {
    var swiper = new Swiper(".js-photos", {
      pagination: {
        el: ".js-photos-pagination",
      },
      navigation: {
        nextEl: ".js-photos-next",
        prevEl: ".js-photos-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        766: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        980: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1024: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1200: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
      },
    });
  }

  $('.js-history-tab').on('click', function(e) {
    e.preventDefault()
    $('.history__content.active').fadeOut().removeClass('active')
    $('.js-history-tab.active').removeClass('active')
    $(this).addClass('active')
    let href = $(this).attr('href')
    $(href).fadeIn().addClass('active')
  })

  $('.js-tabs-item').on('click', function(e) {
    e.preventDefault()
    $('.tabs__content.active').fadeOut().removeClass('active')
    $('.js-tabs-item.active').removeClass('active')
    $(this).addClass('active')
    let href = $(this).attr('href')
    $(href).fadeIn().addClass('active')
  })

  $('.js-header-city-active').on('click', function(){
    $(this).toggleClass('open')
    $('.js-header-city-content').slideToggle()
  })


  $(".js-gondolas-tab").on("click", function (e) {
    e.preventDefault();
    $(".js-gondolas-tab.active").removeClass("active");
    $(this).addClass("active");
    let href = $(this).attr("href");
    $(".gondolas__item.active").fadeOut().removeClass("active");
    $(href).fadeIn().addClass("active");
  });

});
